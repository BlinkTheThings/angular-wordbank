import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Word Bank';
  randomWords : string[];

  constructor() {
    this.randomWords = ['one', 'two'];
  }
}
