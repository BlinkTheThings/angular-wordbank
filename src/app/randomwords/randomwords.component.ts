import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-randomwords',
  templateUrl: './randomwords.component.html',
  styleUrls: ['./randomwords.component.css']
})
export class RandomWordsComponent implements OnInit {

  @Input() words: string[];

  constructor() {
  }

  ngOnInit() {
  }

}
