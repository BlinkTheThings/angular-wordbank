import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomWordsComponent } from './randomwords.component';
import { Component } from '@angular/core';

describe('RandomWordsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RandomWordsWrapper,
        RandomWordsComponent
      ]
    })
    .compileComponents();
  }));

  it('should create the component', () => {
    const fixture: ComponentFixture<RandomWordsComponent> = TestBed.createComponent(RandomWordsComponent);
    const component: RandomWordsComponent = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should have no words when created without an input', () => {
    const fixture: ComponentFixture<RandomWordsComponent> = TestBed.createComponent(RandomWordsComponent);
    const component: RandomWordsComponent = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.words).toBeUndefined();
  });

  it('should let you supply an initial set of words', () => {
    const fixture: ComponentFixture<RandomWordsWrapper> = TestBed.createComponent(RandomWordsWrapper);
    const componentWrapper: RandomWordsWrapper = fixture.componentInstance;
    const component: RandomWordsComponent = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
    expect(component.words).toBe(componentWrapper.inputWords);
  });
});

@Component ({
  selector: 'randomwords-wrapper',
  template: '<app-randomwords [words]="inputWords"></app-randomwords>'
})
class RandomWordsWrapper {
  inputWords: string[] = ['red', 'green'];
}